package Iwona.pl;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Stream;
import org.junit.Test;

public class Numbers {

    @Test
    public void generate(){

         Stream.iterate(0, n -> n+1)
                .filter(number -> number > 100 && number <1000 && number % 5 ==0)
                .peek(x -> System.out.println(x))
                .limit(10)
                .map(number -> number * 3)
                .forEach(System.out::println);
    }

}
