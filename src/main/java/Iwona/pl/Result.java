package Iwona.pl;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Result {
    public static void main(String[] args) {
        Stream<MatchResult> resultStream = createStream();
        System.out.println("Wyniki wszystkich meczy");
        printSortedGoal(resultStream);

        resultStream = createStream();
        System.out.println("\n" + "Wszystkie spotkania Polski");
        printFilteredTeam(resultStream, "Polska").forEach(System.out::println);

        resultStream = createStream();
        System.out.println("\n" + "Liczba wszystkich dróżyn: " + distinctTeams(resultStream));

        resultStream = createStream();
        System.out.println("\n" + "liczba goli");
        System.out.println(allGoals(resultStream));
    }

    public static Stream<MatchResult> createStream() {
        return Stream.of(new MatchResult("Polska", "Irlandia", 2, 0),
                new MatchResult("Brazylia", "Francja", 0, 3),
                new MatchResult("Włochy", "Polska", 2, 1),
                new MatchResult("Brazylia", "Argentyna", 2, 2),
                new MatchResult("Anglia", "Niemcy", 1, 2),
                new MatchResult("Anglia", "Francja", 3, 0),
                new MatchResult("Polska", "Portugalia", 1, 0),
                new MatchResult("Brazylia", "Niemcy", 3, 3));
    }

    public static void printSortedGoal(Stream<MatchResult> result) {
        Stream<MatchResult> sortedResult = result
//                .sorted((result1, result2) -> Integer.compare(result1.getGoalDiff(), result2.getGoalDiff()));
                .sorted(Comparator.comparingInt(MatchResult::getGoalDiff));
        sortedResult.forEach(System.out::println);
    }

    public static List<MatchResult> printFilteredTeam(Stream<MatchResult> results, String team) {
        return results.filter(element -> element.containsTeam(team))
                .collect(Collectors.toList());
    }

    public static long distinctTeams(Stream<MatchResult> results) {
        return results.map(MatchResult::getTeamNames)
                .flatMap(Arrays::stream)
                .distinct()
                .count();
    }

    public static int allGoals(Stream<MatchResult> results) {
//        results.mapToInt(element -> element.getAllGoals())
        return results.mapToInt(MatchResult::getAllGoals)
                .sum();

    }

}
